This is the simplest possible project I could think of. Its only purpose is to figure out _exactly_ how to publish and use npm modules in advance of a larger project.

Just clone the repo. Then...
```
yarn install
# or npm install
npm run start
```
Then serve the project from the root.